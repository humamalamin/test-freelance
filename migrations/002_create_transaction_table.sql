CREATE TABLE `transaction`(
    id VARCHAR(64) NOT NULL,
    account_id INT(11) NOT NULL,
    transaction_date datetime NOT NULL,
    description VARCHAR(255) NOT NULL,
    method VARCHAR(1) NOT NULL DEFAULT 'D',
    amount DECIMAL(10,2) NOT NULL DEFAULT 0,
    primary key (id),
    Foreign key (account_id) references nasabah(account_id)
);

CREATE INDEX idx_transaction ON transaction (id);