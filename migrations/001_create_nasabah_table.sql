CREATE TABLE `nasabah` (
    id VARCHAR(64) NOT NULL,
    account_id INT(11) NOT NULL,
    name VARCHAR(255) NOT NULL,
    primary key (account_id)
);

CREATE INDEX idx_nasabah ON nasabah (id);