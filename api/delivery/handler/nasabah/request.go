package nasabah

import "errors"

type RequestNasabah struct {
	AccountID int    `json:"account_id"`
	Name      string `json:"name"`
}

func (data *RequestNasabah) Validate() error {
	if data.AccountID == 0 {
		return errors.New("account_id is required")
	}

	if data.Name == "" {
		return errors.New("name is required")
	}

	return nil
}
