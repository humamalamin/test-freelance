package nasabah

import (
	"encoding/json"
	"net/http"
	"test-freelance/api/domain/entity/nasabah"
	nasabahInterface "test-freelance/api/domain/interface/nasabah"
	nasabahService "test-freelance/api/service/nasabah"
	jsonHelper "test-freelance/helpers/response"
	"test-freelance/package/manager"

	"github.com/rs/zerolog/log"
)

type NasabahHandler struct {
	Service nasabahInterface.NasabahService
}

// AddNasabah implements nasabah.NasabahHandler
func (nh *NasabahHandler) AddNasabah() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		var request RequestNasabah
		if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
			code := "[Handler] AddNasabah-1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "4000", err.Error())
			return
		}

		err := request.Validate()
		if err != nil {
			code := "[Handler] AddNasabah-2"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "4000", err.Error())
			return
		}

		resultMappingData := mappingRequestToEntity(request)

		if err := nh.Service.AddNasabah(ctx, resultMappingData); err != nil {
			code := "[Handler] AddEmploye-3"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "5000", err.Error())
			return
		}

		jsonHelper.SuccessResponse(w, r, true, "1001", nil, nil)
	})
}

// GetNasabah implements nasabah.NasabahHandler
func (nh *NasabahHandler) GetNasabah() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		results, err := nh.Service.GetNasabah(ctx)
		if err != nil {
			code := "[Handler] GetNasabah-1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "4000", err.Error())
			return
		}

		mappingData := mappintListNasabah(results)

		jsonHelper.SuccessResponse(w, r, true, "1000", mappingData, nil)
	})
}

func NewNasabahHandler(mgr manager.Manager) nasabahInterface.NasabahHandler {
	handler := new(NasabahHandler)
	handler.Service = nasabahService.NewNasabahService(mgr)

	return handler
}

func mappintListNasabah(listdata []nasabah.NasabahModel) []ResponListNasabah {
	var listResponNasabah []ResponListNasabah

	for _, ld := range listdata {
		listResponNasabah = append(listResponNasabah, ResponListNasabah{
			AccountID: ld.AccountID,
			Nama:      ld.Name,
		})
	}

	return listResponNasabah
}

func mappingRequestToEntity(req RequestNasabah) nasabah.NasabahModel {
	return nasabah.NasabahModel{
		Name:      req.Name,
		AccountID: req.AccountID,
	}
}
