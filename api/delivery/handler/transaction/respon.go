package transaction

type ResponseReportPoint struct {
	AccountID  int     `json:"AccountId"`
	Name       string  `json:"Name"`
	TotalPoint float64 `json:"Total Point"`
}

type ResponReport struct {
	TransactionDate string `json:"Transaction Date"`
	Description     string `json:"Description"`
	Credit          string `json:"Credit"`
	Debit           string `json:"Debit"`
	Amount          string `json:"Amount"`
}
