package transaction

import (
	"encoding/json"
	"errors"
	"net/http"
	"test-freelance/api/domain/entity/transaction"
	transactionInterface "test-freelance/api/domain/interface/transaction"
	transactionService "test-freelance/api/service/transaction"
	"test-freelance/helpers/conversion"
	jsonHelper "test-freelance/helpers/response"
	"test-freelance/package/manager"
	"time"

	"github.com/rs/zerolog/log"
)

type TransactionHandler struct {
	Service transactionInterface.TransactionService
}

// GetReportTransaction implements transaction.TransactionHandler
func (th *TransactionHandler) GetReportTransaction() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		accountIDStr := r.FormValue("AccountId")
		startDateStr := r.FormValue("StartDate")
		endDateStr := r.FormValue("EndDate")

		var reqParam RequestParamReport
		accountID, err := conversion.StrToInt(accountIDStr)
		if err != nil {
			code := "[Handler] GetReportTransaction-1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "4000", err.Error())
			return
		}
		reqParam.AccountID = accountID

		startDate, err := time.Parse("02/01/2006", startDateStr)
		if err != nil {
			code := "[Handler] GetReportTransaction-2"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "4000", err.Error())
			return
		}
		reqParam.StartDate = startDate

		endDate, err := time.Parse("02/01/2006", endDateStr)
		if err != nil {
			code := "[Handler] GetReportTransaction-3"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "4000", err.Error())
			return
		}
		reqParam.EndDate = endDate

		if err := reqParam.ValidateParam(); err != nil {
			code := "[Handler] GetReportTransaction-4"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "4000", err.Error())
			return
		}

		results, err := th.Service.GetReportTransaction(ctx, reqParam.AccountID, reqParam.StartDate, reqParam.EndDate)
		if err != nil {
			code := "[Handler] GetReportTransaction-1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "4000", err.Error())
			return
		}

		mappingRespon := mappingReport(results)

		jsonHelper.SuccessResponse(w, r, true, "1000", mappingRespon, nil)

	})
}

// GetDebitTransaction implements transaction.TransactionHandler
func (th *TransactionHandler) GetDebitTransaction() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		results, err := th.Service.GetDebitTransaction(ctx)
		if err != nil {
			code := "[Handler] GetDebitTransaction-1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "4000", err.Error())
			return
		}

		mappingData := mappingResponDebitPoint(results)

		jsonHelper.SuccessResponse(w, r, true, "1000", mappingData, nil)
	})
}

// AddTransaction implements transaction.TransactionHandler
func (th *TransactionHandler) AddTransaction() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		var request RequestTransaction
		if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
			code := "[Handler] AddTransaction-1"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "4000", err.Error())
			return
		}

		err := request.Validate()
		if err != nil {
			code := "[Handler] AddTransaction-2"
			log.Error().Err(err).Msg(code)
			jsonHelper.ErrorResponse(w, r, false, "4000", err.Error())
			return
		}

		resultMapping := mappingRequestToEntity(request)
		if err := th.Service.AddTransaction(ctx, resultMapping); err != nil {
			code := "[Handler] AddTransaction-3"
			log.Error().Err(err).Msg(code)
			if err.Error() == "404" {
				jsonHelper.ErrorResponse(w, r, false, "4004", errors.New("account id not found").Error())
			} else {
				jsonHelper.ErrorResponse(w, r, false, "5000", err.Error())
			}
			return
		}

		jsonHelper.SuccessResponse(w, r, true, "1001", nil, nil)
	})
}

func NewTransactionHandler(mgr manager.Manager) transactionInterface.TransactionHandler {
	handler := new(TransactionHandler)
	handler.Service = transactionService.NewTransactionService(mgr)

	return handler
}

func mappingRequestToEntity(data RequestTransaction) transaction.TransactionModel {
	dateString, _ := time.Parse("2006-01-02", data.TransactionDate)
	var result transaction.TransactionModel
	result.AccountID = data.AccountID
	result.Method = data.Method
	result.Amount = data.Amount
	result.TransactionDate = dateString
	result.Description = data.Description

	return result
}

func mappingResponDebitPoint(data []transaction.TransactionDebitPoint) []ResponseReportPoint {
	var responReportPoint []ResponseReportPoint

	for _, dt := range data {
		totalPoint := countingPoint(dt)
		responReportPoint = append(responReportPoint, ResponseReportPoint{
			AccountID:  dt.AccountID,
			Name:       dt.Name,
			TotalPoint: totalPoint,
		})
	}

	return responReportPoint
}

func countingPoint(data transaction.TransactionDebitPoint) float64 {
	var upperHundred float64
	if data.TotalAmount > 100000 {
		upperHundred = ((data.TotalAmount - 100000) / 2000) * 2
		return 0 + 25 + upperHundred
	}

	if data.TotalAmount > 50000 && data.TotalAmount <= 100000 {
		upperHundred = ((data.TotalAmount - 50000) / 2000) * 1
		return 0 + upperHundred
	}

	return 0
}

func mappingReport(req []transaction.TransactionModel) []ResponReport {
	var listReports []ResponReport
	for _, rq := range req {
		var listReport ResponReport
		if rq.Method == "D" {
			listReport.Debit = conversion.ConvertToRpCurrency(int64(rq.Amount))
		} else {
			listReport.Credit = conversion.ConvertToRpCurrency(int64(rq.Amount))
		}
		listReport.Amount = conversion.ConvertToRpCurrency(int64(rq.Amount))
		listReport.Description = rq.Description
		listReport.TransactionDate = rq.TransactionDate.Format("2006-01-02")
		listReports = append(listReports, listReport)
	}

	return listReports
}
