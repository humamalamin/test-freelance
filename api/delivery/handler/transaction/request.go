package transaction

import (
	"errors"
	"time"
)

type RequestTransaction struct {
	AccountID       int     `json:"account_id"`
	Amount          float64 `json:"amount"`
	Description     string  `json:"description"`
	Method          string  `json:"method"`
	TransactionDate string  `json:"transaction_date"`
}

type RequestParamReport struct {
	AccountID int       `json:"account_id"`
	StartDate time.Time `json:"start_date"`
	EndDate   time.Time `json:"end_date"`
}

func (data *RequestTransaction) Validate() error {
	if data.AccountID == 0 {
		return errors.New("account_id is required")
	}

	if data.Description == "" {
		return errors.New("description is required")
	}

	if data.Method == "" {
		return errors.New("method is required")
	}

	if len(data.Method) > 1 || len(data.Method) < 1 {
		return errors.New("method must be 1 character")
	}

	if data.Method != "C" && data.Method != "D" {
		return errors.New("method must be C or D")
	}

	return nil
}

func (data *RequestParamReport) ValidateParam() error {
	if data.AccountID == 0 {
		return errors.New("account_id is required")
	}

	return nil
}
