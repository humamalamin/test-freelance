package transaction

import (
	"test-freelance/package/manager"

	"github.com/gorilla/mux"

	transactionHandler "test-freelance/api/delivery/handler/transaction"
)

func NewTransactionRoute(mgr manager.Manager, route *mux.Router) {
	transactionHandler := transactionHandler.NewTransactionHandler(mgr)

	route.Handle("", transactionHandler.AddTransaction()).Methods("POST")
	route.Handle("/point", transactionHandler.GetDebitTransaction()).Methods("GET")
	route.Handle("/report", transactionHandler.GetReportTransaction()).Methods("GET")
}
