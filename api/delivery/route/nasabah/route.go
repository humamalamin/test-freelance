package nasabah

import (
	"test-freelance/package/manager"

	"github.com/gorilla/mux"

	nasabahHandler "test-freelance/api/delivery/handler/nasabah"
)

func NewNasabahRoute(mgr manager.Manager, route *mux.Router) {
	nasabahHandler := nasabahHandler.NewNasabahHandler(mgr)

	route.Handle("", nasabahHandler.GetNasabah()).Methods("GET")
	route.Handle("", nasabahHandler.AddNasabah()).Methods("POST")
}
