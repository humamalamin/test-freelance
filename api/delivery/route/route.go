package route

import (
	nasabahRoute "test-freelance/api/delivery/route/nasabah"
	transactionRoute "test-freelance/api/delivery/route/transaction"
	"test-freelance/package/manager"

	"github.com/gorilla/mux"
)

func NewRoutes(r *mux.Router, mgr manager.Manager) {
	apiNasabah := r.PathPrefix("/nasabah").Subrouter()

	nasabahRoute.NewNasabahRoute(mgr, apiNasabah)

	apiTransaction := r.PathPrefix("/transaction").Subrouter()
	transactionRoute.NewTransactionRoute(mgr, apiTransaction)
}
