package nasabah

import (
	"context"
	"database/sql"
	nasabahEntity "test-freelance/api/domain/entity/nasabah"
	nasabahInterface "test-freelance/api/domain/interface/nasabah"

	"github.com/google/uuid"
)

type nasabahRepo struct {
	DB *sql.DB
}

// AddNasabah implements nasabah.NasabahRepository
func (np *nasabahRepo) AddNasabah(ctx context.Context, request nasabahEntity.NasabahModel) error {
	id := uuid.New().String()
	_, err := np.DB.Exec("INSERT INTO nasabah (id, account_id, name) VALUES (?, ?, ?)", id, request.AccountID, request.Name)
	if err != nil {
		return err
	}

	return nil
}

// GetNasabah implements nasabah.NasabahRepository
func (np *nasabahRepo) GetNasabah(ctx context.Context) ([]nasabahEntity.NasabahModel, error) {
	rows, err := np.DB.Query("SELECT * FROM nasabah")
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var listNasabah []nasabahEntity.NasabahModel

	for rows.Next() {
		var nasabah nasabahEntity.NasabahModel

		err := rows.Scan(&nasabah.ID, &nasabah.AccountID, &nasabah.Name)
		if err != nil {
			return nil, err
		}

		listNasabah = append(listNasabah, nasabah)
	}

	return listNasabah, nil
}

func NewNasabahRepository(db *sql.DB) nasabahInterface.NasabahRepository {
	repo := new(nasabahRepo)
	repo.DB = db

	return repo
}
