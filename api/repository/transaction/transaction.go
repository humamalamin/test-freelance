package transaction

import (
	"context"
	"database/sql"
	"errors"
	"test-freelance/api/domain/entity/transaction"
	transactionInterface "test-freelance/api/domain/interface/transaction"
	"time"

	"github.com/google/uuid"
)

type transactionRepo struct {
	DB *sql.DB
}

// GetReportTransaction implements transaction.TransactionRepository
func (tr *transactionRepo) GetReportTransaction(ctx context.Context, accountID int, startDate, endDate time.Time) ([]transaction.TransactionModel, error) {
	var listReportTransaction []transaction.TransactionModel

	mainQuery := `SELECT id, account_id, method, amount, description, transaction_date FROM transaction WHERE account_id = ?`

	if !startDate.IsZero() && !endDate.IsZero() {
		mainQuery += ` AND transaction_date BETWEEN ? AND ?`
	}

	rows, err := tr.DB.Query(mainQuery, accountID, startDate.AddDate(0, 0, -1).Format("2006-01-02"), endDate.Format("2006-01-02"))
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var transaction transaction.TransactionModel

		err := rows.Scan(&transaction.ID, &transaction.AccountID, &transaction.Method, &transaction.Amount, &transaction.Description, &transaction.TransactionDate)
		if err != nil {
			return nil, err
		}

		listReportTransaction = append(listReportTransaction, transaction)
	}

	return listReportTransaction, nil
}

// GetDebitTransaction implements transaction.TransactionRepository
func (tr *transactionRepo) GetDebitTransaction(ctx context.Context) ([]transaction.TransactionDebitPoint, error) {
	var listTransaction []transaction.TransactionDebitPoint
	rows, err := tr.DB.Query("SELECT SUM(amount) as total_amount, name, n.account_id FROM transaction JOIN nasabah n on transaction.account_id = n.account_id WHERE method = 'D' GROUP BY n.account_id")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var transaction transaction.TransactionDebitPoint

		err := rows.Scan(&transaction.TotalAmount, &transaction.Name, &transaction.AccountID)
		if err != nil {
			return nil, err
		}

		listTransaction = append(listTransaction, transaction)
	}

	return listTransaction, nil
}

// AddTransaction implements transaction.TransactionRepository
func (tr *transactionRepo) AddTransaction(ctx context.Context, request transaction.TransactionModel) error {
	count, err := tr.GetNasabahByAccountID(request.AccountID)
	if err != nil {
		return err
	}

	if count == 0 {
		return errors.New("404")
	}

	id := uuid.New().String()

	_, err = tr.DB.Exec("INSERT INTO transaction (id, account_id, amount, description, method, transaction_date) VALUES (?, ?, ?, ?, ?, ?)", id, request.AccountID, request.Amount, request.Description, request.Method, request.TransactionDate)
	if err != nil {
		return err
	}

	return nil
}

func (tr *transactionRepo) GetNasabahByAccountID(acID int) (int, error) {
	var count int
	err := tr.DB.QueryRow("SELECT COUNT(id) FROM nasabah WHERE account_id = ?", acID).Scan(&count)
	if err != nil {
		return 0, err
	}

	return count, nil
}

func NewTransactionRepository(db *sql.DB) transactionInterface.TransactionRepository {
	repo := new(transactionRepo)
	repo.DB = db

	return repo
}
