package nasabah

import (
	"context"
	"net/http"
	"test-freelance/api/domain/entity/nasabah"
)

type NasabahRepository interface {
	GetNasabah(ctx context.Context) ([]nasabah.NasabahModel, error)
	AddNasabah(ctx context.Context, request nasabah.NasabahModel) error
}

type NasabahService interface {
	GetNasabah(ctx context.Context) ([]nasabah.NasabahModel, error)
	AddNasabah(ctx context.Context, request nasabah.NasabahModel) error
}

type NasabahHandler interface {
	GetNasabah() http.Handler
	AddNasabah() http.Handler
}
