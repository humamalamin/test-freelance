package transaction

import (
	"context"
	"net/http"
	"test-freelance/api/domain/entity/transaction"
	"time"
)

type TransactionRepository interface {
	AddTransaction(ctx context.Context, request transaction.TransactionModel) error
	GetDebitTransaction(ctx context.Context) ([]transaction.TransactionDebitPoint, error)
	GetReportTransaction(ctx context.Context, accountID int, startDate, endDate time.Time) ([]transaction.TransactionModel, error)
}

type TransactionService interface {
	AddTransaction(ctx context.Context, request transaction.TransactionModel) error
	GetDebitTransaction(ctx context.Context) ([]transaction.TransactionDebitPoint, error)
	GetReportTransaction(ctx context.Context, accountID int, startDate, endDate time.Time) ([]transaction.TransactionModel, error)
}

type TransactionHandler interface {
	AddTransaction() http.Handler
	GetDebitTransaction() http.Handler
	GetReportTransaction() http.Handler
}
