package transaction

import "time"

type TransactionModel struct {
	ID              string
	AccountID       int
	Amount          float64
	Description     string
	Method          string
	TransactionDate time.Time
}

type TransactionDebitPoint struct {
	TotalAmount float64
	Name        string
	AccountID   int
}
