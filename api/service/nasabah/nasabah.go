package nasabah

import (
	"context"
	"test-freelance/api/domain/entity/nasabah"
	nasabahInterface "test-freelance/api/domain/interface/nasabah"
	nasabahRepository "test-freelance/api/repository/nasabah"
	"test-freelance/package/manager"
)

type NasabahService struct {
	repo nasabahInterface.NasabahRepository
}

// AddNasabah implements nasabah.NasabahService
func (ns *NasabahService) AddNasabah(ctx context.Context, request nasabah.NasabahModel) error {
	err := ns.repo.AddNasabah(ctx, request)
	if err != nil {
		return err
	}

	return nil
}

// GetNasabah implements nasabah.NasabahService
func (ns *NasabahService) GetNasabah(ctx context.Context) ([]nasabah.NasabahModel, error) {
	listNasabah, err := ns.repo.GetNasabah(ctx)
	if err != nil {
		return nil, err
	}

	return listNasabah, nil
}

func NewNasabahService(mgr manager.Manager) nasabahInterface.NasabahService {
	service := new(NasabahService)
	service.repo = nasabahRepository.NewNasabahRepository(mgr.GetMysql())

	return service
}
