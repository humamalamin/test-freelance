package transaction

import (
	"context"
	"test-freelance/api/domain/entity/transaction"
	transactionInterface "test-freelance/api/domain/interface/transaction"
	transactionRepository "test-freelance/api/repository/transaction"
	"test-freelance/package/manager"
	"time"
)

type TransactionService struct {
	repo transactionInterface.TransactionRepository
}

// GetReportTransaction implements transaction.TransactionService
func (ts *TransactionService) GetReportTransaction(ctx context.Context, accountID int, startDate, endDate time.Time) ([]transaction.TransactionModel, error) {
	listTransaction, err := ts.repo.GetReportTransaction(ctx, accountID, startDate, endDate)
	if err != nil {
		return nil, err
	}

	return listTransaction, nil
}

// GetDebitTransaction implements transaction.TransactionService
func (tr *TransactionService) GetDebitTransaction(ctx context.Context) ([]transaction.TransactionDebitPoint, error) {
	listTransaction, err := tr.repo.GetDebitTransaction(ctx)
	if err != nil {
		return nil, err
	}

	return listTransaction, nil
}

// AddTransaction implements transaction.TransactionService
func (ts *TransactionService) AddTransaction(ctx context.Context, request transaction.TransactionModel) error {
	err := ts.repo.AddTransaction(ctx, request)
	if err != nil {
		return err
	}
	return err
}

func NewTransactionService(mgr manager.Manager) transactionInterface.TransactionService {
	service := new(TransactionService)
	service.repo = transactionRepository.NewTransactionRepository(mgr.GetMysql())

	return service
}
