package conversion

import (
	"strconv"
	"strings"
	"time"

	"github.com/dustin/go-humanize"
)

// Convert String Value Into Int
func StrToInt(data string) (int, error) {
	newData, err := strconv.Atoi(data)
	if err != nil {
		return 0, err
	}

	return newData, nil
}

// Convert String Value Into Int64
func StrToInt64(data string) (int64, error) {
	newData, err := strconv.ParseInt(data, 10, 64)
	if err != nil {
		return 0, err
	}

	return newData, nil
}

// Convert Phone Number Into +62 Format
func ConvertPhoneNumber(data string) string {
	if data[0:1] == "0" {
		data = "+62" + data[1:]
	}
	if data[:2] == "62" {
		data = "+" + data
	}

	return data
}

// Convert Time Into TZ
func ConvertTimeToTZ(data time.Time, timezone string) time.Time {
	loc, _ := time.LoadLocation(timezone)

	newData := data.In(loc)

	return newData
}

// Convert To RP Currency
func ConvertToRpCurrency(data int64) string {
	humanizeValue := humanize.CommafWithDigits(float64(data), 0)
	stringValue := strings.Replace(humanizeValue, ",", ".", -1)

	return stringValue
}
