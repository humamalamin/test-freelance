package response

import (
	"encoding/json"
	"net/http"
	loggingHelper "test-freelance/helpers/log"
	paginationHelper "test-freelance/helpers/pagination"
)

// Return JSON
func WriteJSON(w http.ResponseWriter, code int, v interface{}) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	enc := json.NewEncoder(w)
	enc.SetEscapeHTML(false)
	return enc.Encode(v)
}

// Return JSON Success V3
func SuccessResponse(w http.ResponseWriter, r *http.Request, status bool, code string, data interface{}, pagination *paginationHelper.Page) {
	meta := &meta{
		Status:     status,
		Message:    "success",
		Code:       "OK",
		Pagination: pagination,
	}

	res := &response{
		Meta: *meta,
		Data: data,
	}

	loggingHelper.Addlog(r, "SUCCESS", data)

	WriteJSON(w, http.StatusOK, res)
}

// Return JSON Error
func ErrorResponse(w http.ResponseWriter, r *http.Request, status bool, code string, message interface{}) {
	meta := &meta{
		Code:    code,
		Status:  status,
		Message: message,
	}

	res := &response{
		Meta: *meta,
		Data: nil,
	}

	loggingHelper.Addlog(r, "ERROR", message)

	WriteJSON(w, http.StatusBadRequest, res)
}
