package manager

import (
	"database/sql"
	"test-freelance/helpers/pagination"
	"test-freelance/package/config"
	"test-freelance/package/database"
	"test-freelance/package/server"

	"github.com/rs/zerolog/log"
)

type Manager interface {
	GetConfig() *config.Config
	GetServer() *server.Server
	GetMysql() *sql.DB
	GetPagination() pagination.Pagination
}

type manager struct {
	config     *config.Config
	server     *server.Server
	dbMysql    *sql.DB
	pagination pagination.Pagination
}

func NewInit() (Manager, error) {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Error().Err(err).Msg("[NewInit-1] Failed to Initialize Configuration")
		return nil, err
	}

	srv := server.NewServer(cfg)

	dbMysql, err := database.NewMysql(cfg).Connect()
	if err != nil {
		log.Error().Err(err).Msg("[NewInit-2] Failed to Initialize Database Mysql")
		return nil, err
	}

	paginationHelper := pagination.NewPagination()

	return &manager{
		config:     cfg,
		server:     srv,
		dbMysql:    dbMysql,
		pagination: paginationHelper,
	}, nil
}

func (sm *manager) GetConfig() *config.Config {
	return sm.config
}

func (sm *manager) GetServer() *server.Server {
	return sm.server
}

func (sm *manager) GetMysql() *sql.DB {
	return sm.dbMysql
}

func (sm *manager) GetPagination() pagination.Pagination {
	return sm.pagination
}
