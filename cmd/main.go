package main

import (
	"fmt"
	"os"
	"test-freelance/package/manager"
	"test-freelance/package/server"
	"time"

	masterRoutes "test-freelance/api/delivery/route"
)

func run() error {
	mgr, err := manager.NewInit()
	if err != nil {
		return err
	}

	tzLocation, err := time.LoadLocation(mgr.GetConfig().AppTz)
	if err != nil {
		return err
	}
	time.Local = tzLocation

	server := server.NewServer(mgr.GetConfig())
	masterRoutes.NewRoutes(server.Router, mgr)

	// routes

	server.RegisterRouter(server.Router)

	return server.ListenAndServe()
}

func main() {
	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}

}
